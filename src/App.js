import React from 'react';
import './App.css';
import TodoList from './Component/TodoList'
import {useState} from 'react'
import TodoItem from './Component/TodoItem'

function App() {
  const [todos,setTodos] = useState([])
  const addTodo =(text) =>{
    let id = 1;
    if(todos.length > 0){
      id = todos[0].id + 1;
    }
    let todo = {id: id , text: text,complete: false}
    let newTodos =[todo, ...todos]
    setTodos(newTodos)
    localStorage.setItem('name', JSON.stringify(newTodos))
  };
    const removeTodo = (id) =>{
      let updateTodos =[...todos].filter((todo) =>todo.id !== id)
      setTodos(updateTodos)
      let arrayName = JSON.parse(localStorage.getItem('name'))
      let index = arrayName.filter((todo) =>todo.id !== id)
      localStorage.setItem('name', JSON.stringify(index))

    }
    const completeTodo = (id) =>{
      let updateTodos = todos.map((todo) =>{
        if (todo.id === id ){
          todo.complete =! todo.complete
        }
        return todo
      })
      setTodos(updateTodos)
    }

  
  return (
    <div className="App">
     <TodoList addTodo={addTodo} />
     <hr className="seperator"></hr>
     {
       todos.map((todo) =>{
        return (
        <TodoItem removeTodo ={removeTodo} completeTodo ={completeTodo} todo ={todo} key={todo.id}/>
        )
       })
     }
    </div>
  );
}

export default App;
