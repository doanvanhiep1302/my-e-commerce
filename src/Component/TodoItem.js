import React, { Component } from 'react'
import {AiOutlineCheck} from 'react-icons/ai'
import {RiDeleteBin6Line} from 'react-icons/ri'

export default function TodoItem(props)  {
    const {todo,removeTodo,completeTodo} = props
    return (
      
      <div className={todo.complete ?"todo-row complete": "todo-row "}>
      <div className="list-items" onClick ={() => completeTodo(todo.id)}>
      <div style ={todo.complete ?{display: "inline-block"}:{display: "none"}} 
      className="check-icon">
      <AiOutlineCheck  
      />
      </div>
      <div className="item-name" >
      {todo.text}
      </div>
      </div>
        
        <span className="delete-icon"><RiDeleteBin6Line  onClick={() => {removeTodo(todo.id)}}/></span>
      </div>
    )
  
}
