import React, { useState, useEffect } from 'react';
import logo from './logo-is.webp';
import { FaSearch } from 'react-icons/fa';
import { FaAngleRight } from 'react-icons/fa';
import { AiOutlineStar } from 'react-icons/ai';
import { AiFillStar } from 'react-icons/ai';
import { uniqBy } from "lodash";
import { FiSearch } from 'react-icons/fi';
import axios from "axios";


const renderStar = (rate) => {
    const splitRating = rate.slice(0, 1);
    const listStar = [];
    for (let i = 0; i < 5; i++) {
        if (i < splitRating) {
            listStar.push(<span className='star-rating'>
                <AiFillStar className="star-rating" /></span>)
        }
        else {
            listStar.push(<span className='star-rating'>
                <AiOutlineStar className="star-rating" /></span>)
        }
    }
    return listStar;
}
export default function Product() {

    const [data, setData] = useState([]);
    const [value, setValue] = useState("");
    const [sortValue, setSortValue] = useState("");
    const [currentPage, setCurrentPage] = useState(0);
    const [pageLimit] = useState(16);
    const [filterData,setFilterData] = useState([]);
    // const [buttonOpen,setButtonOpen] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get('http://localhost:3004/products')
            setData(result.data)
        }
        fetchData()
    }, [])
    
    useEffect(() => {
        loadProductData(0, 16, 0);
    }, []);

    const loadProductData = async (start, end, increase) => {
        return await axios
            .get(`http://localhost:3004/product?_start=${start}&_end=${end}`)
            .then((response) => {
                setData(response.data);
                setCurrentPage(currentPage + increase);

            })
            .catch((err) => console.log(err))
    };

    // console.log("data", data);
  
    const handleSearch = async (e) => {
        e.preventDefault();
        return await axios.get(`http://localhost:3004/product?q=${value}`)
            .then((response) => {
                setData(response.data)
                setValue(value);
            })
            .catch((err) => console.log(err))
    }

    const handleSortAsc = async (value) => {
        setSortValue(value);
        return await axios
            .get(`http://localhost:3004/product?_sort=price&_order=asc`)
            .then((response) => {
                setData(response.data);
                console.log(response.data, value)
            })
            .catch((err) => console.log(err));
    }

    const handleSortDesc = async (value) => {

        setSortValue(value);
        return await axios
            .get(`http://localhost:3004/product?_sort=price&_order=desc`)
            .then((response) => {
                setData(response.data);
                console.log(value)
            })
            .catch((err) => console.log(err));
    }
    const handleFilter = async (value) => {
        // setButtonOpen(!buttonOpen)
        return await axios
            .get(`http://localhost:3004/product?q=${value}`)
            .then((response) => {
                setFilterData(response.data);
                console.log(value)
            })
            .catch((err) => console.log(err));
    }


    const handleSort = async (e) => {
        const value = e.target.value
        if (value === "asc") {
            handleSortAsc(value)
        } else if (value === "desc") {
            handleSortDesc(value)
        }
    }

    const renderPagination = () =>{
        if(currentPage === 0) {
            return (
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                    <a class="page-link" style={{color: '#000'}} href="#" >Prev</a>
                    <li class="page-item"><a onClick={() => loadProductData(0,16,0)} class="page-link" href="#">1</a></li>
                    <li class="page-item">
                    <a onClick={() => loadProductData((currentPage + 1)*16,(currentPage + 2)*16,1)} class="page-link" href="#">Next</a>
                    </li>
                    </ul>
                </nav>

            )
        }else if(currentPage < pageLimit -1 && data.length === pageLimit)  {
            return (
            <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <li class="page-item">
                    <a onClick={() => loadProductData((currentPage -1)*16, currentPage * 16, -1)} class="page-link" href="#">Prev</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">{currentPage + 1}</a></li>
                <li class="page-item">
                    <a onClick={() => loadProductData((currentPage + 1)*16,(currentPage + 2)*16,1)} class="page-link" href="#">Next</a>
                </li>
                </ul>
            </nav>
            )
        }
        else{
            return (
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                <li class="page-item">
                <a onClick={() => loadProductData((currentPage -1)*16, currentPage * 16, -1)} class="page-link" href="#">Prev</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">{currentPage + 1}</a></li>
                </ul>
            </nav>
            )
        }
    }

    return (
        <div>
            <div id="header">
                <div>
                    <img className="logo-icon" src={logo} alt="logo" />
                </div>
                <div className="logo-name">
                    <a href="./">amazing</a>
                </div>
                <div className="form-input" >
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Search a product"
                        value={value}
                        onChange={(e) => setValue(e.target.value)}
                    />
                    <button type="submit" className="btn-search" onClick={handleSearch}><FaSearch /></button>
                </div>
            </div>
            <div id="content">
                <div className="side-bar">
                    <div className="side-bar-wrapper">
                        <div><a  href ="./" className="clear-filter"> Clear all filter</a></div>
                           <div className="side-bar-title">Show results for</div>
                        {
                            uniqBy(data, 'type').map((product) => (
                                
    
                                <div onClick ={() => handleFilter(product.type)} className="side-bar-name font-12 " ><i><FaAngleRight /></i>{product.type} 
                                                   
                                </div>
                            )
                            )
                        }
                    </div>
                    <div className="side-bar-wrapper">
                        <div className="side-bar-title">Refine by</div>
                        <div className="side-bar-list-item">
                            <div className="child-title font-12">Type</div>
                            <a href="#" className="side-item font-12">

                                {
                                    uniqBy(data, 'detail-type').map((product) => (

                                        <div className="side-bar-name font-12">
                                            <input type="checkbox" className="checkbox" />
                                            <span onClick ={() => handleFilter(product['detail-type'])} >{product['detail-type']}</span>
                                            <span className="item-count">({product.count})</span>
                                        </div>


                                    ))
                                }

                            </a>
                        </div>
                        <div className="side-bar-list-item">
                            <div className="child-title font-12">Brand</div>

                            <div className="child-search">
                                <span><FiSearch /></span>
                                <input type="text" className="child-input" placeholder="Search for other ..." /></div>
                            <a href="#" className="side-item font-12">
                                {
                                    uniqBy(data, 'brand').map((product) => (

                                        <div className="side-bar-name font-12">
                                            <input type="checkbox" className="checkbox" />
                                            <span onClick={() =>handleFilter(product['brand'])}>{product['brand']}</span>
                                            <span   className="item-count">({product.count})</span>
                                        </div>


                                    ))
                                }

                            </a>
                        </div>
                        <div className="side-bar-list-item">
                            <div className="child-title font-12">Rating</div>
                            <div>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span className="star-emty font-12">& Up </span>
                                <span className="rating count font-12">16.074</span>
                            </div>
                            <div>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span className="star-emty font-12">& Up </span>
                                <span className="rating count font-12">17.696</span>
                            </div>
                            <div>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span className="star-emty font-12">& Up </span>
                                <span className="rating count font-12">17.890</span>
                            </div>
                            <div>
                                <span><AiFillStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span><AiOutlineStar className="star-rating" /></span>
                                <span className="star-emty font-12">& Up </span>
                                <span className="rating count font-12">18.046</span>
                            </div>

                        </div>
                        <div className="side-bar-list-item">
                            <div className="child-title font-12">Price</div>
                            <div className="font-12">≤ 1</div>
                            <div className="font-12">$1 - 80</div>
                            <div className="font-12">$80 - 160</div>
                            <div className="font-12">$160 - 240</div>
                            <div className="font-12">$240 - 1.820</div>
                            <div className="font-12">$1.820 - 3.400</div>
                            <div className="font-12">$3.400 - 4.980</div>
                            <div className="font-12">≥ $4.980</div>

                        </div>
                        <div>
                            <form className="price-ranges">
                                <label className="ranges-label">
                                    <span className="ranges-currency">$</span>
                                    <input type="number" className="ranges-input" min ="0" max ="100" />
                                </label>
                                <span className="font-12"> to </span>
                                <label className="ranges-label">
                                    <span className="ranges-currency">$</span>
                                    <input type="number" className="ranges-input"  min ="0" max ="100" />
                                </label>
                                <button className ="ais-price-ranges">Go</button>

                            </form>
                            <div className="side-footer">Data courtesy of <a href="https://developer.bestbuy.com/">BestBuy</a></div>
                        </div>
                    </div>
                </div>
                <div className="content-wraper">
                    <div className="sort-by">
                        <label >Sort by </label>
                        <div className="sort-by-selector">
                            <select
                                onChange={handleSort}

                            >
                                <option className="ais-sort-by-selector--item" value="default"><a href="./">Featured</a></option>
                                <option className="ais-sort-by-selector--item" value="asc">Price asc.</option>
                                <option className="ais-sort-by-selector--item" value="desc">Price desc.</option>

                            </select>
                        </div>
                    </div>
                    <div className="product">
                        {
                            (filterData.length > 0 ? filterData : data).map((product) => (

                                <div key={product.id} className='hit'>

                                    <div className="product-picture-wraper">
                                        <div className='product-picture'>
                                            <img className="product-img" src={product.image} />
                                        </div>
                                    </div>
                                    <div className='product-desc-wrapper'>
                                        <div className='product-name'>{product.title}</div>
                                        <div className="product-price">${product.price}</div>
                                        <div className='product-ratting'>{renderStar(product.rating)}</div>

                                    </div>
                                </div>
                            ))}
                    </div>
                </div>
                <div className="render-pagination">{renderPagination()}</div>
            </div>
        </div>

    )
}
